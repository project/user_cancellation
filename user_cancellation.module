<?php


/**
 * @file
 * Lets users cancel their own account
 *
 * Admin can specify email to confirm cancellation
 * Admin can specify node content to be deleted on user cancellation
 */

/**
 * Implementation of hook_menu().
 */
function user_cancellation_menu($may_cache) {

  global $user;

  $items = array();
  $delete_access = (user_access('delete own account') && $user->uid == arg(1));

  if ($may_cache) {
    $items[] = array(
      'path' => 'admin/user/user_cancellation',
      'title' => t('User cancellation settings'),
      'description' => t('Change user cancellation settings.'),
      'callback' => 'drupal_get_form',
      'callback arguments' => array('user_cancellation_settings'),
      'access' => user_access('administer site configuration'),
    );
  }
  else {
    $items[] = array(
      'path' => 'myaccount/'. arg(1) .'/delete',
      'title' => t('Delete'),
      'callback' => 'delete_user',
      'access' => $delete_access,
      'type' => MENU_CALLBACK,
    );
  }

  if ($delete_access && $_POST['op'] == t('Delete my account')) {
    drupal_goto("myaccount/$user->uid/delete");
  }

  return $items;

}

/**
 * Define the settings form.
 */
function user_cancellation_settings() {

  $form['cancel_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Retain nodes'),
  );

  $form['cancel_settings']['user_cancellation'] = array(
    '#type' => 'checkboxes',
    '#title' => t('On user cancellation, retain these node types'),
    '#options' => node_get_types('names'),
    '#default_value' => variable_get('user_cancellation', array('story', 'page')),
    '#description' => t('When a user cancels the following node types will be retained (and user id will be set to 0).'),
  );

  $form['email'] = array(
    '#type' => 'fieldset',
    '#title' => t('Cancel confirmation email'),
  );

  $form['email']['send_confirmation'] = array(
    '#type' => 'checkbox',
    '#title' => t('Send user confirmation when cancelling account.'),
    '#options' => node_get_types('names'),
    '#default_value' => variable_get('send_confirmation', true),
    '#description' => t('Send an email to user confirming their account has been cancelled.'),
  );

  $form['email']['user_cancellation_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject of user cancellation e-mail'),
    '#default_value' => _cancellation_mail_text('subject'),
    '#maxlength' => 180,
    '#description' => t('Customize the subject of the user cancellation e-mail.') .' '. t('Available variables are:') .' !username, !site, !login_url, !uri, !uri_brief, !mailto, !date, !login_uri, !edit_uri.',
  );
  $form['email']['user_cancellation_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Body of user cancellation e-mail'),
    '#default_value' => _cancellation_mail_text('body'),
    '#rows' => 15,
    '#description' => t('Customize the body of the user cancellation e-mail.') .' '. t('Available variables are:') .' !username, !site, !login_url, !uri, !uri_brief, !mailto, !login_uri, !edit_uri.',
  );

  $form['array_filter'] = array('#type' => 'hidden');
  return system_settings_form($form);
}

function _cancellation_mail_text($messageid, $variables = array()) {

  // Check if an admin setting overrides the default string.
  if ($admin_setting = variable_get('user_cancellation_'. $messageid, FALSE)) {
    return strtr($admin_setting, $variables);
  }
  // No override, return with default strings.
  else {
    switch ($messageid) {
      case 'subject' :
        return t('Your account with !site has been cancelled', $variables);
        break;
      case 'body' :
        return t("!username,\n\nYour account at !site has been cancelled. Thank you for your custom.\n\n\n--  !site team", $variables);
        break;
    }
  }

}

/**
 * Implementation of hook_perm().
 */
function user_cancellation_perm() {
  return array('delete own account');
}

/**
 * Implementation of hook_form_alter().
 */
function user_cancellation_form_alter($form_id, & $form) {

  global $user;

  switch ($form_id) {

    case 'user_edit' :

      //if the user has access to delete content or administer users, add a delete button
      if (user_access('delete own account') && $user->uid == arg(1)) {
        $form['delete'] = array(
          '#type' => 'submit',
          '#value' => t('Delete my account'),
          '#weight' => 31,
        );
      }

      break;

  }

  return $form;

}

function user_cancellation_user($op, & $edit, & $account, $category = NULL) {

  global $base_url;
  $from = variable_get('site_mail', ini_get('sendmail_from'));

  if ($op == 'delete') {

    //pass in an array of types to exclude from deletion process
    $types = variable_get('user_cancellation', null);
    divorce_children($account->uid, $types);

    $confirm_email['subject'] = variable_get('user_cancellation_subject', null);
    $confirm_email['body'] = variable_get('user_cancellation_body', null);

    // send cancel confirmation email

    if (variable_get('send_confirmation', null)) {
      $variables = array(
        '!username' => $account->name,
        '!site' => variable_get('site_name', 'Drupal'),
        '!login_url' => user_pass_reset_url($account),
        '!uri' => $base_url,
        '!uri_brief' => substr($base_url, strlen('http://')),
        '!mailto' => $account->mail,
        '!date' => format_date(time()),
        '!login_uri' => url('user', NULL, NULL, TRUE),
        '!edit_uri' => url('user/'. $account->uid .'/edit', NULL, NULL, TRUE),
      );
      $subject = _cancellation_mail_text('subject', $variables);
      $body = _cancellation_mail_text('body', $variables);
      $mail_success = drupal_mail('user-canx', $account->mail, $subject, $body, $from);

      if (!$mail_success) {
        watchdog('user', t('Cancel confirmation email failed for '. $account->name), WATCHDOG_ERROR);
      }

    }

  }

}

//function which will divorce children of speficic user (uid)

function divorce_children($uid, $type = null) {

  if (is_array($type)) {

    foreach ($type as $t) {
      $cond[] = "type = '%s'";
      $arguments[] = $t;
    }

    $cond = implode(' OR ', $cond);
    $arguments[] = $uid;

    db_query("UPDATE {node} SET uid = 0 WHERE (". $cond .") AND uid = %d", $arguments);

  }
  else {
    db_query("UPDATE {node} SET uid = 0 WHERE uid = %d", $uid);
  }

}

/* allow users to delete themselves from drupal */

function delete_user() {

  global $user;

  $account = user_load(array('uid' => arg(1)));

  if (user_access('delete own account') && $user->uid == arg(1)) {

    if ($account === FALSE) {
      drupal_set_message(t('The account does not exist or has already been deleted.'));
      drupal_goto();
    }
    $edit = $_POST['op'] ? $_POST : (array) $account;

    if (arg(2) == 'delete') {
      if ($edit['confirm']) {
        user_delete($edit, $account->uid);
        drupal_goto();
      }
      else {
        return drupal_get_form('user_confirm_delete', $account->name, $account->uid);
      }
    }

  }
  else {
    drupal_goto("admin/user/user");
  }

}
