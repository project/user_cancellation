This module allows users to cancel their own account.
Admin can select which nodes to retain after a user deletes their account.
Admin can select whether to send an email confirming cancellation & specify the text of the cancellation email


INSTALL
--------------------------------------------------------------------------------
- Drop the entire directory into your modules directory:
    Drupal 5+ - sites/all/modules or sites/my_site/modules

- Enable the module:
    Drupal 5+ - admin/build/modules



CONFIGURATION
--------------------------------------------------------------------------------

- Enable the "delete own account" permission in Admin->UserManagement->AccessControl.
- When installed, admin can select which nodes to keep after a user cancels their account. 
- In the admin settings /admin/user/user_cancellation you can select whether to send a cancellation email and the text of that email.

AUTHOR:
-------------
Ben Scott
ben@benscott.co.uk



